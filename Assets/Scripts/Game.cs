﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class Game : MonoBehaviour
{
    #region Singleton

    private static Game _instance;

    public static Game Instance
    {
        get
        {
            if (_instance == null)
            {
                Debug.LogWarning($"You need to add an instance of {typeof(Game)} to your scene!");
                return null;
            }

            return _instance;
        }
    }

    #endregion

    public List<string> ActiveRecipes => _activeRecipes;

    public MergableItem DraggableObjectPrefab;
    public GridHandler MainGrid;
    public GridHandler MixingGrid;
    public GameSettings GameSettings;

    private List<string> _activeRecipes = new List<string>();

    private void Awake()
    {
        if (_instance == null)
            _instance = this;
        else
            Debug.LogWarning(
                $"A {typeof(Game)} instance already exists! Make sure you only have one {typeof(Game)} instance at a time.");

        Screen.fullScreen =
            false; // https://issuetracker.unity3d.com/issues/game-is-not-built-in-windowed-mode-when-changing-the-build-settings-from-exclusive-fullscreen

        InitializeMainGridHandler();
        InitializeMixingGridHandler();

        // load all item definitions
        ItemUtils.InitializeMap(GameSettings.RecipesAndRawIngredientsList.ToArray());
    }

    private void Start()
    {
        ReloadLevel(GameSettings.GameDifficulty);
    }

    private void OnDestroy()
    {
        ItemUtils.ClearMap();
    }

    private void InitializeMainGridHandler()
    {
        MainGrid.Initialize(GameSettings.MainGridSize);
    }

    private void InitializeMixingGridHandler()
    {
        MixingGrid.Initialize(GameSettings.MixingGridSize);
    }

    public void ReloadLevel(int difficulty = 1)
    {
        // clear the board
        var fullCells = MainGrid.FullCellsList.ToArray();
        for (int i = fullCells.Length - 1; i >= 0; i--)
            MainGrid.ClearCell(fullCells[i]);

        // choose new recipes
        _activeRecipes.Clear();
        difficulty = Mathf.Max(difficulty, 1);
        for (int i = 0; i < difficulty; i++)
        {
            // a 'recipe' has more than 1 ingredient, else it is just a raw ingredient.
            var randomRecipe = ItemUtils.RecipeMap.RandomEntry(kvp => kvp.Value.Count > 1).Key;
            _activeRecipes.Add(randomRecipe);
        }

        PopulateBoard();
    }

    private void PopulateBoard()
    {
        int itemsToSpawnCount = ItemsToSpawnCount();

        List<GridCell> emptyCellsClone = new List<GridCell>();
        foreach (var cell in MainGrid.EmptyCellsList)
            emptyCellsClone.Add(cell);

        while (emptyCellsClone.Count > itemsToSpawnCount)
            emptyCellsClone.Remove(emptyCellsClone.RandomEntry());

        foreach (var cell in emptyCellsClone)
            cell.TrySpawnItem();
    }

    private int ItemsToSpawnCount()
    {
        int count = Mathf.RoundToInt(GameSettings.MainGridElementsCount * GameSettings.ItemDensityPercentage / 100);

        if (count <= 0 || count > GameSettings.MainGridElementsCount)
        {    
            count = 0;
            Debug.LogWarning(
                $"A main grid size of: {GameSettings.MainGridSize}, with an item density percentage of " +
                $"{GameSettings.ItemDensityPercentage} results in {count} items being spawned!");
        }

        return count;
    }
}