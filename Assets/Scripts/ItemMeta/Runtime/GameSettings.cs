﻿using System.Collections.Generic;
using System.Linq;
using GramGames.CraftingSystem.DataContainers;
using UnityEditor;
using UnityEngine;

[CreateAssetMenu(menuName = "Game/Settings")]
public class GameSettings : ScriptableObject
{
    public List<NodeContainer> RecipesList
    {
        get => _recipesList;
        set
        {
            _recipesList = value;
            RefreshRecipesAndRawIngredientsList();
        }
    }
    public List<NodeContainer> RawIngredientsList
    {
        get => _rawIngredientsList;
        set
        {
            _rawIngredientsList = value;
            RefreshRecipesAndRawIngredientsList();
        }
    }
    public List<NodeContainer> RecipesAndRawIngredientsList
    {
        get
        {
            if (_recipesAndRawIngredientsList == null || _recipesAndRawIngredientsList.Count == 0)
                RefreshRecipesAndRawIngredientsList();

            return _recipesAndRawIngredientsList;
        }
        set => _recipesAndRawIngredientsList = value;
    }
    
    [Range(0, 100)] public float ItemDensityPercentage = 100.0f;
    public int GameDifficulty = 1;

    [Space] public Vector2Int MainGridSize = new Vector2Int(6, 5);
    public Vector2Int MixingGridSize = new Vector2Int(3, 2);

    [Space] [SerializeField] private List<NodeContainer> _recipesList;
    [SerializeField] private List<NodeContainer> _rawIngredientsList;

    public int MainGridElementsCount => MainGridSize.x * MainGridSize.y;

    private List<NodeContainer> _recipesAndRawIngredientsList;
    
    private void RefreshRecipesAndRawIngredientsList()
    {
        _recipesAndRawIngredientsList = GetRecipesAndRawIngredientsList();
    }

    private List<NodeContainer> GetRecipesAndRawIngredientsList()
    {
        return _recipesList.Union(_rawIngredientsList).ToList();
    }

#if UNITY_EDITOR
    
    private void OnValidate()
    {
        RefreshRecipesAndRawIngredientsList();
    }

    public void ValidateRecipesList()
    {
        RecipesList = RecipesList.Where(item => item != null).ToList();
        RecipesList = RecipesList.Where(item => !item.IsRawMaterial()).ToList();
    }

    public void ValidateRawIngredientsList()
    {
        RawIngredientsList = RawIngredientsList.Where(item => item != null).ToList();
        RawIngredientsList = RawIngredientsList.Where(item => item.IsRawMaterial()).ToList();
    }

    public void AddAllRecipes()
    {
        RecipesList.Clear();
        var assetsList = GetAllNodeContainerScriptables();

        foreach (var item in assetsList)
        {
            if (item.IsRawMaterial()) continue;
            RecipesList.Add(item);
        }
    }

    public void AddAllRawIngredients()
    {
        RawIngredientsList.Clear();
        var assetsList = GetAllNodeContainerScriptables();

        foreach (var item in assetsList)
        {
            if (!item.IsRawMaterial()) continue;
            RawIngredientsList.Add(item);
        }
    }

    private List<NodeContainer> GetAllNodeContainerScriptables()
    {
        var nodeContainers = AssetDatabase.FindAssets("t:NodeContainer");
        List<NodeContainer> nodeContainersList = new List<NodeContainer>();
        foreach (var item in nodeContainers)
        {
            Debug.Log(AssetDatabase.GUIDToAssetPath(item));
            var nodeContainer = AssetDatabase.LoadAssetAtPath<NodeContainer>(AssetDatabase.GUIDToAssetPath(item));
            nodeContainersList.Add(nodeContainer);
        }

        return nodeContainersList;
    }

#endif
}