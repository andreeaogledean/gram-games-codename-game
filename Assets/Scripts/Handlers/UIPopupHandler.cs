﻿using UnityEngine;

public class UIPopupHandler : MonoBehaviour
{
    [SerializeField] private Canvas _popupsCanvas = null;
    [SerializeField] private UIPopupDatabase _uiPopupDatabase = null;

    private void Start()
    {
        UIButtonsHandler.Instance.OnUniqueButtonClicked += UIButtonsHandler_OnUniqueButtonClicked;
    }

    private void OnDestroy()
    {
        UIButtonsHandler.Instance.OnUniqueButtonClicked -= UIButtonsHandler_OnUniqueButtonClicked;
    }

    private void UIButtonsHandler_OnUniqueButtonClicked(UIButtonsHandler.UniqueButtonType buttonType)
    {
        switch (buttonType)
        {
            case UIButtonsHandler.UniqueButtonType.Options:
                OpenPopup(OptionsUIPopup.OptionsPopupName);
                break;
        }
    }

    public void OpenPopup(string name)
    {
        var popupPrefab = _uiPopupDatabase.GetPopup(name);

        Instantiate(popupPrefab, _popupsCanvas.transform);
    }
}
