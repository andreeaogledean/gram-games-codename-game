﻿using UnityEngine;
using UnityEngine.UI;

public class UIButton : MonoBehaviour
{
    public UIButtonsHandler.UniqueButtonType Type = UIButtonsHandler.UniqueButtonType.None;
    public Button Button;

    private void Start()
    {
        UIButtonsHandler.Instance.Subscribe(this);
    }

    private void OnDestroy()
    {
        UIButtonsHandler.Instance.Unsubscribe(this);
    }
}
