﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[CreateAssetMenu(menuName = "Database/UI Popup Database")]
public class UIPopupDatabase : ScriptableObject
{
    public List<UIPopupBase> UIPopupsList => _uiPopupsList;

    [SerializeField] private List<UIPopupBase> _uiPopupsList = new List<UIPopupBase>();
    
    public void ValidateList()
    {
        _uiPopupsList = _uiPopupsList.Where(item => item != null).ToList();
        _uiPopupsList = _uiPopupsList.Where(item => !string.IsNullOrEmpty(item.PopupName)).ToList();
    }

    public UIPopupBase GetPopup(string name)
    {
        foreach (var popup in UIPopupsList)
        {
            if (popup.PopupName == name)
                return popup;
        }
        
        Debug.LogWarning($"Could not find popup {name} in {nameof(UIPopupDatabase)}!");
        return null;
    }
}
