﻿using System;
using UnityEngine;

public class UIButtonsHandler : MonoBehaviour
{
    public event Action<UniqueButtonType> OnUniqueButtonClicked;

    public static UIButtonsHandler Instance = null;

    public enum UniqueButtonType
    {
        None,
        Start,
        Options,
        BackToMainMenu
    }

    private void Awake()
    {
        if (Instance == null)
            Instance = this;
    }

    public void Subscribe(UIButton uiButton)
    {
        switch (uiButton.Type)
        {
            case UniqueButtonType.Start:
                uiButton.Button.onClick.AddListener(StartButton_OnClick);
                break;
            case UniqueButtonType.BackToMainMenu:
                uiButton.Button.onClick.AddListener(BackToMainMenuButton_OnClick);
                break;
            case UniqueButtonType.Options:
                uiButton.Button.onClick.AddListener(OptionsButton_OnClick);
                break;
        }
    }

    public void Unsubscribe(UIButton uiButton)
    {
        switch (uiButton.Type)
        {
            case UniqueButtonType.Start:
                uiButton.Button.onClick.RemoveListener(StartButton_OnClick);
                break;
            case UniqueButtonType.BackToMainMenu:
                uiButton.Button.onClick.RemoveListener(BackToMainMenuButton_OnClick);
                break;
            case UniqueButtonType.Options:
                uiButton.Button.onClick.RemoveListener(OptionsButton_OnClick);
                break;
        }
    }

    private void StartButton_OnClick()
    {
        OnUniqueButtonClicked?.Invoke(UniqueButtonType.Start);
    }

    private void BackToMainMenuButton_OnClick()
    {
        OnUniqueButtonClicked?.Invoke(UniqueButtonType.BackToMainMenu);
    }

    private void OptionsButton_OnClick()
    {
        OnUniqueButtonClicked?.Invoke(UniqueButtonType.Options);
    }
}