﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class UIPopupBase : MonoBehaviour
{
    private const float ShowPopupAnimationDuration = 0.3f;
    private const float HidePopupAnimationDuration = 0.15f;

    public string PopupName;

    [Space]
    [SerializeField] private Transform _popupContentTransform = null;
    [SerializeField] private Button _closePopupButton = null;
    
    [Space]
    [SerializeField] private AnimationCurve _showPopupAnimationCurve = AnimationCurve.Linear(0, 0, 1, 1);
    [SerializeField] private AnimationCurve _hidePopupAnimationCurve = AnimationCurve.Linear(0, 0, 1, 1);

    private Vector3 _contentInitialLocalScale;

    #region Awake / Start / OnDestroy
    
    protected virtual void Awake()
    {
        _contentInitialLocalScale = _popupContentTransform.localScale;
        
        _closePopupButton.onClick.AddListener(ClosePopupButton_OnClick);
    }

    protected virtual void Start()
    {
        ShowPopup();
    }

    protected virtual void OnDestroy()
    {
        DOTween.Kill(_popupContentTransform);
        _closePopupButton.onClick.RemoveListener(ClosePopupButton_OnClick);
    }
    
    #endregion
    
    #region ClosePopupButton_OnClick

    private void ClosePopupButton_OnClick()
    {
        HidePopup();
    }

    #endregion
    
    #region ShowPopup / HidePopup / DestroyPopup
    
    public void ShowPopup()
    {
        _popupContentTransform.localScale = Vector3.zero;
        _popupContentTransform.DOScale(_contentInitialLocalScale, ShowPopupAnimationDuration)
            .SetEase(_showPopupAnimationCurve);
    }

    public void HidePopup()
    {
        _popupContentTransform.localScale = _contentInitialLocalScale;
        _popupContentTransform.DOScale(Vector3.zero, HidePopupAnimationDuration)
            .SetEase(_hidePopupAnimationCurve).onComplete += DestroyPopup;
    }

    public void DestroyPopup()
    {
        Destroy(gameObject);
        Destroy(this);
    }
    
    #endregion
}