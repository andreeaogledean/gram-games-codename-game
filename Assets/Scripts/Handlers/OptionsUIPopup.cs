﻿using UnityEngine;
using UnityEngine.UI;

public class OptionsUIPopup : UIPopupBase
{
    private const string MusicVolumePlayerPrefsName = "MusicVolume";
    
    public const string OptionsPopupName = "OptionsPopup";
    
    [SerializeField] private Slider _musicVolumeSlider = null;

    #region OnValidate / Awake / OnDestroy
    
    private void OnValidate()
    {
        PopupName = OptionsPopupName;

        if (_musicVolumeSlider == null) _musicVolumeSlider = GetComponentInChildren<Slider>();
    }

    protected override void Awake()
    {
        base.Awake();
        
        ChangeVolume(GetVolumeFromPlayerPrefs());
        
        _musicVolumeSlider.onValueChanged.AddListener(VolumeSlider_OnValueChanged);
    }

    protected override void OnDestroy()
    {
        base.OnDestroy();
        
        _musicVolumeSlider.onValueChanged.RemoveListener(VolumeSlider_OnValueChanged);
    }
    
    #endregion
    
    #region VolumeSlider_OnValueChanged

    private void VolumeSlider_OnValueChanged(float value)
    {
        SetMusicVolume(value);
    }
    
    #endregion
    
    #region ChangeVolume / SetMusicVolume

    public void ChangeVolume(float value)
    {
        _musicVolumeSlider.value = value;
        SetMusicVolume(value);
    }

    private void SetMusicVolume(float value)
    {
        //Set Volume To AudioSource
        SaveVolumeToPlayerPrefs();
    }
    
    #endregion
    
    #region SaveVolumeToPlayerPrefs / GetVolumeFromPlayerPrefs

    private void SaveVolumeToPlayerPrefs()
    {
        PlayerPrefs.SetFloat(MusicVolumePlayerPrefsName, _musicVolumeSlider.value);
    }

    private float GetVolumeFromPlayerPrefs()
    {
        return PlayerPrefs.GetFloat(MusicVolumePlayerPrefsName, 1.0f);
    }
    
    #endregion
}
