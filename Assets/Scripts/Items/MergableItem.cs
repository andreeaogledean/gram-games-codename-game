﻿using GramGames.CraftingSystem.DataContainers;
using UnityEngine;
using UnityEngine.UI;

public class MergableItem : DraggableObject
{
    public Image image;

    [SerializeField] private BoxCollider2D _boxCollider2D = null;

    private Image _image;
    private GridCell _parentCell;
    private RayCastHandler<GridCell> _rayCast;
    private Transform _parentWhileDragged;

    private NodeContainer _itemData; // this is our item definition
    public NodeContainer ItemData => _itemData;

    public LayerMask mask;

    private void Awake()
    {
        _rayCast = new RayCastHandler<GridCell>(transform);
    }

    /// <summary>
    /// Should be called immediately after instantiation!
    /// </summary>
    public void Configure(NodeContainer item, GridCell current, Transform parentWhileDragged)
    {
        _itemData = item;
        _parentWhileDragged = parentWhileDragged;

        if (_itemData != null)
            image.sprite = _itemData.MainNodeData.Sprite;
        else
            image.sprite = null;

        AssignToCell(current);
    }

    public void AssignToCell(GridCell current)
    {
        if (_parentCell != null && _parentCell.GridHandler != null)
            _parentCell.GridHandler.EmptyCell(_parentCell);

        _parentCell = current;

        transform.SetParent(current.transform);
        transform.position = current.transform.position;
        current.SetItemAssigned(this);

        var rectTransform = transform as RectTransform;
        if (rectTransform == null) return;
        
        _boxCollider2D.size = rectTransform.rect.size;
    }

    protected override void DoBeginDrag()
    {
        transform.SetParent(_parentWhileDragged);
    }

    protected override void DoEndDrag()
    {
        if (_rayCast.RayCastDown(mask))
        {
            //we hit a slot
            var targets = _rayCast.GetTargets();
            foreach (var slot in targets)
            {
                if (slot.HasItem == false)
                {
                    Debug.Log("end");
                    AssignToCell(slot);
                    return;
                }
            }

            ReturnToPreviousSlot();
        }
        else
        {
            ReturnToPreviousSlot();
        }
    }

    private void ReturnToPreviousSlot()
    {
        if (_parentCell != null)
        {
            Debug.Log("off grid");
            AssignToCell(_parentCell);
        }
    }

    public GridCell GetCurrentCell()
    {
        return _parentCell;
    }
}