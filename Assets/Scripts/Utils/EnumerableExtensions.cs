﻿using System;
using System.Collections.Generic;
using System.Linq;
using Random = System.Random;

public static class EnumerableExtensions
{
    private static Random rng = new Random();

    public static T RandomEntry<T>(this IEnumerable<T> source, Func<T, bool> predicate)
    {
        if (source == null)
            throw new ArgumentNullException($"Argument {nameof(source)} is null!");
        if (predicate == null)
            throw new ArgumentNullException($"Argument {nameof(predicate)} is null!");

        var randomSource = source.Shuffle();
        
        foreach (T sourceEntry in randomSource)
        {
            if (predicate(sourceEntry))
                return sourceEntry;
        }

        return default;
    }

    public static T RandomEntry<T>(this IEnumerable<T> source)
    {
        if (source == null)
            throw new ArgumentNullException($"Argument {nameof(source)} is null!");
        
        var randomSource = source.Shuffle();
        
        return randomSource.FirstOrDefault();
    }

    public static IEnumerable<T> Shuffle<T>(this IEnumerable<T> source)
    {
        if (source == null) throw new ArgumentNullException(nameof(source));
        
        return source.Shuffle(rng);
    }

    public static IEnumerable<T> Shuffle<T>(this IEnumerable<T> source, Random rng)
    {
        if (source == null) throw new ArgumentNullException(nameof(source));
        if (rng == null) throw new ArgumentNullException(nameof(rng));

        return source.ShuffleIterator(rng);
    }

    private static IEnumerable<T> ShuffleIterator<T>(this IEnumerable<T> source, Random rng)
    {
        var buffer = source.ToList();
        for (int i = 0; i < buffer.Count; i++)
        {
            int j = rng.Next(i, buffer.Count);
            yield return buffer[j];

            buffer[j] = buffer[i];
        }
    }
}