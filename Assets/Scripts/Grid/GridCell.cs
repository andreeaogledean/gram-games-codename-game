﻿using System.Linq;
using GramGames.CraftingSystem.DataContainers;
using UnityEngine;

public enum MoveDirection
{
    Left,
    Right,
    Up,
    Down
}

public class GridCell : MonoBehaviour
{
    [SerializeField] private GridCell _left;
    [SerializeField] private GridCell _right;
    [SerializeField] private GridCell _up;
    [SerializeField] private GridCell _down;
    [SerializeField] private GridHandler _handler;
    [SerializeField] private Transform _itemParentWhileDragged = null;
    
    private MergableItem _item;
    public MergableItem Item => _item;
    public bool HasItem => _item != null;
    public GridHandler GridHandler => _handler;

    public void Initialize(GridHandler gridHandler, Transform parentWhileDragged)
    {
        _itemParentWhileDragged = parentWhileDragged;
        SetHandler(gridHandler);
    }

    public void TrySpawnItem()
    {
        var chosenRecipe = Game.Instance.ActiveRecipes[Random.Range(0, Game.Instance.ActiveRecipes.Count)];
        var ingredients = ItemUtils.RecipeMap[chosenRecipe].ToArray();
        var ingredient = ingredients[Random.Range(0, ingredients.Length)];
        var item = ItemUtils.ItemsMap[ingredient.NodeGUID];
        
        SpawnItem(item);
    }

    public void SpawnItem(NodeContainer item)
    {
	    _handler.ClearCell(this);

		var obj = Instantiate(Game.Instance.DraggableObjectPrefab, transform);
	    obj.Configure(item, this, _itemParentWhileDragged);
    }
    
    public void SetHandler(GridHandler handler)
    {
        _handler = handler;
    }

    public void SetItemAssigned(MergableItem item)
    {
	    if (_handler == null)
		    _handler = GetComponentInParent<GridHandler>();
       
        if (_item != null)
            _handler.SetCellState(_item.GetCurrentCell(), true);
        
        _item = item;
        _handler.SetCellState(this, _item == null);
    }

    public void SetNeighbor(MoveDirection direction, GridCell neighbor)
    {
        switch (direction)
        {
            case MoveDirection.Left:
                _left = neighbor;
                break;
            case MoveDirection.Right:
                _right = neighbor;
                break;
            case MoveDirection.Up:
                _up = neighbor;
                break;
            case MoveDirection.Down:
                _down = neighbor;
                break;
        }
    }

    public void EmptyItem()
    {
        _item = null;
    }

    public void ClearItem()
    {
        if (_item != null)
        {
            Destroy(_item.gameObject);
        }
    }
}
