﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class GridHandler : MonoBehaviour
{
    #region Fields

    [SerializeField] private Vector2Int _gridSize = new Vector2Int(6, 5);
    [SerializeField] private GridLayoutGroup _gridLayoutGroup = null;
    [SerializeField] private GridCell _gridCellPrefab = null;
    [SerializeField] private Transform _gridCellsParent = null;
    [SerializeField] protected Transform _itemParentWhileDragged = null;

    #endregion

    #region Private / Protected Properties
    
    protected List<GridCell> _emptyCellsList = new List<GridCell>();
    protected List<GridCell> _fullCellsList = new List<GridCell>();
    
    #endregion
    
    #region Getters
    
    public List<GridCell> FullCellsList => _fullCellsList;
    public List<GridCell> EmptyCellsList => _emptyCellsList;

    #endregion
    
    #region Initialize / GetSafeIndex / CellIsOnLeftEdge / CellIsOnRightEdge

    public void Initialize()
    {
        Initialize(_gridSize);
    }

    public void Initialize(Vector2Int gridSize)
    {
        _gridSize = gridSize;
        SetGridLayoutGroupProperties();

        int elementsCount = gridSize.x * gridSize.y;

        ClearExistingCells();

        GridCell[] cells = new GridCell[elementsCount];

        for (int i = 0; i < elementsCount; i++)
        {
            var newCell = Instantiate(_gridCellPrefab, _gridCellsParent);
            newCell.Initialize(this, _itemParentWhileDragged);
            newCell.name = "Grid Cell " + i;

            var leftGridCellIndex = GetSafeIndex(i - 1, i);
            var upGridCellIndex = GetSafeIndex(i - gridSize.x, i);
            if (leftGridCellIndex >= 0)
            {
                if (!CellIsOnLeftEdge(i))
                    newCell.SetNeighbor(MoveDirection.Left, cells[leftGridCellIndex]);
                if (!CellIsOnRightEdge(leftGridCellIndex))
                    cells[leftGridCellIndex].SetNeighbor(MoveDirection.Right, newCell);
            }
            if (upGridCellIndex >= 0)
            {
                cells[upGridCellIndex].SetNeighbor(MoveDirection.Down, newCell);
                newCell.SetNeighbor(MoveDirection.Up, cells[upGridCellIndex]);
            }
            
            cells[i] = newCell;
            _emptyCellsList.Add(newCell);
        }
    }

    private int GetSafeIndex(int index, int upperLimit, int lowerLimit = 0)
    {
        if (index < lowerLimit || upperLimit < index)
            return -1;

        return index;
    }

    private bool CellIsOnLeftEdge(int index)
    {
        if (index % _gridSize.x == 0) return true;
        return false;
    }

    private bool CellIsOnRightEdge(int index)
    {
        if (index % _gridSize.x == _gridSize.x - 1) return true;
        return false;
    }

    #endregion

    #region SetGridLayoutGroupProperties
    
    private void SetGridLayoutGroupProperties()
    {
        _gridLayoutGroup.constraintCount = _gridSize.x;

        Vector2 cellSize = new Vector2(100, 100);
        var rect = (_gridLayoutGroup.transform as RectTransform).rect;
        
        cellSize.x = (rect.width - (_gridSize.x - 1) * _gridLayoutGroup.spacing.x) / _gridSize.x;
        cellSize.y = (rect.height - (_gridSize.y - 1) * _gridLayoutGroup.spacing.y) / _gridSize.y;

        _gridLayoutGroup.cellSize = cellSize;
    }
    
    #endregion

    #region Helpers

    private void ClearExistingCells()
    {
        int childs = _gridCellsParent.childCount;
        for (int i = childs - 1; i >= 0; i--)
        {
            DestroyImmediate(_gridCellsParent.GetChild(i).gameObject);
        }

        _emptyCellsList = new List<GridCell>();
        _fullCellsList = new List<GridCell>();
    }

    public void AddMergeableItemToEmpty(MergableItem item)
    {
        var cell = _emptyCellsList.FirstOrDefault();
        if (cell != null)
        {
            item.AssignToCell(cell);
        }
    }

    public void EmptyCell(GridCell cell)
    {
        if (_fullCellsList.Contains(cell))
        {
            _fullCellsList.Remove(cell);
            cell.EmptyItem();
        }

        if (!_emptyCellsList.Contains(cell))
            _emptyCellsList.Add(cell);
    }

    public void ClearCell(GridCell cell)
    {
        if (_fullCellsList.Contains(cell))
        {
            _fullCellsList.Remove(cell);
            cell.ClearItem();
        }

        if (!_emptyCellsList.Contains(cell))
            _emptyCellsList.Add(cell);
    }


    public void SetCellState(GridCell cell, bool empty)
    {
        if (cell == null) return;
        if (empty)
        {
            if (_fullCellsList.Contains(cell))
            {
                _fullCellsList.Remove(cell);
            }

            if (_emptyCellsList.Contains(cell) == false)
            {
                _emptyCellsList.Add(cell);
            }
        }
        else
        {
            if (_emptyCellsList.Contains(cell))
            {
                _emptyCellsList.Remove(cell);
            }

            if (_fullCellsList.Contains(cell) == false)
            {
                _fullCellsList.Add(cell);
            }
        }
    }

    #endregion
}