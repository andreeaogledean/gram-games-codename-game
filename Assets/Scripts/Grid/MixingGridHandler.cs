﻿using System.Collections.Generic;
using System.Linq;
using GramGames.CraftingSystem.DataContainers;
using UnityEngine;
using UnityEngine.UI;

public class MixingGridHandler : GridHandler
{
	[Header("Mixing Grid")]
	[SerializeField] private MergableItem _itemPrefab = null;
	// [SerializeField] private GridHandler _fallbackMixItem;
	[SerializeField] private Button _mixbutton = null;

	private void Awake()
	{
		_mixbutton.onClick.AddListener(PressMixButton);
	}

	private void OnDestroy()
	{
		_mixbutton.onClick.RemoveListener(PressMixButton);
	}

	public void PressMixButton()
	{
		var mixItems = new Dictionary<GridCell, NodeContainer>();
		foreach (var cell in _fullCellsList)
		{
			if (cell.Item != null)
				mixItems[cell] = cell.Item.ItemData;
		}

		var result = ItemUtils.FindBestRecipe(mixItems.Values.ToArray());
		if (result != null)
		{
			var mixedItemNames = mixItems.Values.ToArray().Select(s => s.MainNodeData.Sprite.name).ToArray();
			Debug.Log($"mix success: [{string.Join(",", mixedItemNames)}] => {result.MainNodeData.Sprite.name}");
			
			// clear the mixed items
			for (int i = _fullCellsList.Count - 1; i >= 0; i--)
				ClearCell(_fullCellsList[i]);

			// create the new item from the recipe
			var instance = Instantiate(_itemPrefab, _emptyCellsList[0].transform);
			instance.Configure(result, _emptyCellsList[0], _itemParentWhileDragged);
			// _fallbackMixItem.AddMergeableItemToEmpty(instance);
		}
		else
		{
			Debug.LogError("mix failed!");
		}
	}
}
