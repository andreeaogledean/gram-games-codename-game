﻿using UnityEditor;
using UnityEngine;

namespace Editor
{
    [CustomEditor(typeof(GameSettings))]
    public class GameSettingsEditor : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            var script = (GameSettings) target;

            if (GUILayout.Button(nameof(script.ValidateRecipesList), GUILayout.Height(23)))
            {
                script.ValidateRecipesList();
            }

            if (GUILayout.Button(nameof(script.ValidateRawIngredientsList), GUILayout.Height(23)))
            {
                script.ValidateRawIngredientsList();
            }

            if (GUILayout.Button(nameof(script.AddAllRecipes), GUILayout.Height(18)))
            {
                script.AddAllRecipes();
            }
            
            if (GUILayout.Button(nameof(script.AddAllRawIngredients), GUILayout.Height(18)))
            {
                script.AddAllRawIngredients();
            }
        }
    }
}