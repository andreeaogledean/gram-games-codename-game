﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(UIPopupDatabase))]
public class UIPopupDatabaseEditor : UnityEditor.Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        var script = (UIPopupDatabase) target;

        if (GUILayout.Button(nameof(script.ValidateList), GUILayout.Height(18)))
        {
            script.ValidateList();
        }
    }
}
