﻿using UnityEditor;
using UnityEngine;

namespace Grid.Editor
{
    [CustomEditor(typeof(GridHandler))]
    public class GridHandlerEditor : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            GridHandler myTarget = (GridHandler)target;
            if (GUILayout.Button(nameof(myTarget.Initialize)))
            {
                myTarget.Initialize();
            }
        }
    }
}