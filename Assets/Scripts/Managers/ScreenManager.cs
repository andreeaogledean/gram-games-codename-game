﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ScreenManager : MonoBehaviour
{
    public const string MainMenuSceneName = "MainMenuScene";
    public const string GameSceneName = "GameScene";

    private const float LoadSceneWithFadeAnimationDuration = 0.7f;
    private const float LoadSceneWithFadeFadeInAnimationDelay = 0.1f;

    public static ScreenManager Instance;

    [SerializeField] private CanvasGroup _canvasGroup = null;

    #region Awake / Start / OnDestroy

    private void Awake()
    {
        if (Instance == null)
            Instance = this;
    }
    
    private void Start()
    {
        UIButtonsHandler.Instance.OnUniqueButtonClicked += UIButtonsHandler_OnUniqueButtonClicked;
    }

    private void OnDestroy()
    {
        UIButtonsHandler.Instance.OnUniqueButtonClicked -= UIButtonsHandler_OnUniqueButtonClicked;

        DOTween.Kill(_canvasGroup);
    }
    
    #endregion
    
    #region UIButtonsHandler_OnUniqueButtonClicked

    private void UIButtonsHandler_OnUniqueButtonClicked(UIButtonsHandler.UniqueButtonType buttonType)
    {
        switch (buttonType)
        {
            case UIButtonsHandler.UniqueButtonType.Start:
                LoadSceneWithFade(GameSceneName);
                break;
            case UIButtonsHandler.UniqueButtonType.BackToMainMenu:
                UnloadSceneWithFade(GameSceneName);
                break;
        }
    }
    
    #endregion
    
    #region LoadScene / UnloadScene

    public void LoadScene(string sceneName)
    {
        SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive);
    }

    public void UnloadScene(string sceneName)
    {
        SceneManager.UnloadSceneAsync(sceneName);
    }
    
    #endregion
    
    #region LoadSceneWithFade / UnloadSceneWithFade

    public void LoadSceneWithFade(string sceneName)
    {
        _canvasGroup.DOFade(1, LoadSceneWithFadeAnimationDuration / 2).onComplete += () => LoadSceneFadeOut(sceneName);
    }

    public void UnloadSceneWithFade(string sceneName)
    {
        _canvasGroup.DOFade(1, LoadSceneWithFadeAnimationDuration / 2).onComplete +=
            () => UnloadSceneFadeOut(sceneName);
    }
    
    #endregion
    
    #region LoadSceneFadeOut / UnloadSceneFadeOut

    private void LoadSceneFadeOut(string sceneName)
    {
        LoadScene(sceneName);
        _canvasGroup.DOFade(0, LoadSceneWithFadeAnimationDuration / 2).SetDelay(LoadSceneWithFadeFadeInAnimationDelay);
    }

    private void UnloadSceneFadeOut(string sceneName)
    {
        UnloadScene(sceneName);
        _canvasGroup.DOFade(0, LoadSceneWithFadeAnimationDuration / 2).SetDelay(LoadSceneWithFadeFadeInAnimationDelay);
    }
    
    #endregion
}